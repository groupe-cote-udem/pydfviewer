=====================
Welcome to pydfviewer
=====================


Installation
============

Create venv, acrtivate and install package::

  $ python3 -m venv venv/pydfviewer
  $ source venv/pydfviewer/bin/activate
  $ pip install -U pip
  $ pip install -e .

Development
===========

Install additionnal packages::

  $ pip install -e .[tests]


Usage
=====

Use command to open pdf / png in a Qt window::

  $ pydfopen some.pdf
