import logging
import os

import fitz

from PyQt5.QtCore import Qt, QSize, QByteArray
from PyQt5.QtGui import QPalette, QPixmap, QScreen, QGuiApplication, QImage
from PyQt5.QtWidgets import QGridLayout, QLabel, QWidget


class MainWidget(QWidget):
    """Main application widget."""

    def __init__(
            self,
            app,
            document: str,
            ):
        """Window init method."""
        QWidget.__init__(self)
        self.app = app
        self.document = document
        self.app.setWindowTitle(self.document)
        self.screen = QGuiApplication.primaryScreen()
        self.pixel_per_inch_x = self.screen.logicalDotsPerInchX()
        self.pixel_per_inch_y = self.screen.logicalDotsPerInchY()
        self.build_gui()

    def build_gui(self):
        """Build the GUI."""
        self.check_document()
        self.app._logger.info(f"Opening: {self.document}")
        # use fitz to open pdf file and generate a fitz pixmap
        # fitz pixmap to byte array
        # from bytearray to qimage
        # qimage to QPixmap
        self.pdf_doc = fitz.open(self.document)
        page0 = self.pdf_doc.load_page(0)
        pixmap = page0.get_pixmap()
        px1 = fitz.Pixmap(pixmap, 0)
        imgdata = px1.tobytes("ppm")
        qimage = QImage()
        w, h = page0.rect.width, page0.rect.height
        qimage.loadFromData(QByteArray(imgdata))
        self.layout = QGridLayout()
        self.setLayout(self.layout)
        self.pdf_label = QLabel()
        # change background color to white
        # palette = QPalette()
        # palette.setColor(QPalette.ColorRole.Window, Qt.GlobalColor.white)
        # self.pdf_label.setAutoFillBackground(True)
        # self.pdf_label.setPalette(palette)
        self.pdf_label.setPixmap(QPixmap.fromImage(qimage))
        self.layout.addWidget(self.pdf_label, 0, 0, 1, 1)

    def check_document(self):
        """Check that document is suitable."""
        if not self.document.endswith(".pdf"):
            raise NotImplementedError(f"Not a pdf: {self.document}")
        if not os.path.isfile(self.document):
            raise FileNotFoundError(self.document)
