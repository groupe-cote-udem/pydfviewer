"""Main app module."""
import logging

from PyQt5.QtWidgets import (
        QMainWindow,
        )

from pyqtgraph.Qt.QtWidgets import QApplication


logging.basicConfig()


# Only one instance allowed.
class PydfViewer(QMainWindow):
    """Main Window for viewer app."""

    app = None

    def __init__(
            self, app: QApplication, *args, loglevel=logging.INFO,
            **kwargs):
        """Create the Main GUI app init method."""
        QMainWindow.__init__(self)
        if self.app is not None:
            raise RuntimeError("Only one app instance allowed!")
        PydfViewer.app = app
        self._logger = logging.getLogger("PydfViewer")
        self._logger.setLevel(loglevel)
        # main widget (contains viewer)
        # import here to avoid import loops
        from .main_widget import MainWidget
        self.centerWidget = MainWidget(self, *args, **kwargs)
        self.setCentralWidget(self.centerWidget)

    @classmethod
    def kill(cls):
        """Kills the application."""
        if cls.app is None:
            raise RuntimeError("No app to kill...")
        cls.app.quit()
