"""pydfopen module."""
import argparse
import logging
import traceback

from PyQt5.QtWidgets import QMessageBox

from pyqtgraph.Qt.QtWidgets import QApplication

from ..app import PydfViewer


def main():
    """Execute the main pydfopen script."""
    parser = argparse.ArgumentParser(
            description=(
                "GUI to visualize lidar point cloud with corresponding img."))
    parser.add_argument(
            "file", action="store",
            help=("The file to open."),
            )
    parser.add_argument(
            "--loglevel", action="store", default=logging.INFO,
            help=("The logging level (defaults to INFO)."))
    args = parser.parse_args()
    try:
        app = QApplication([])
        window = PydfViewer(
                app,
                args.file,
                loglevel=args.loglevel,
                )
        window.show()
        app.exec()
    except KeyboardInterrupt:
        print("App killed :(")
    except Exception as exc:
        errorbox = QMessageBox()
        errorbox.setIcon(QMessageBox.Icon.Critical)
        errorbox.setStandardButtons(QMessageBox.StandardButton.Close)
        errorbox.setText("U DONUT! An error occured!")
        errorbox.setWindowTitle("U PIG!")
        # taken from:
        # https://stackoverflow.com/a/35712784/6362595
        stack = ''.join(
                traceback.TracebackException.from_exception(exc).format())
        print("An error occured:")
        print(stack)
        errorbox.setInformativeText(stack)
        errorbox.buttonClicked.connect(errorbox.close)
        errorbox.exec()
    else:
        print("App exited normally.")


if __name__ == "__main__":
    main()
